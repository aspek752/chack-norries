import AppState from "./AppState";

type IGetState = () => AppState;

export default IGetState;