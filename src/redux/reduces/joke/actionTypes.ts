enum actionTypes {
  ADD_JOKE = 'ADD_JOKE',
  CLEAR_JOKE = 'CLEAR_JOKE',
}

export default actionTypes;